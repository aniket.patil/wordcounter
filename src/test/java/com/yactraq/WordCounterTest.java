package com.yactraq;

import com.yactraq.wordcounter.FileParser;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class WordCounterTest {


    @Test
    public void testFileParser() {
        FileParser fileParser = new FileParser();
        String fileName = getClass().getClassLoader().getResource("test1.txt").getFile();
        try {
            System.out.println("************** Testing File test1.txt ***************\n");
            BufferedReader bufferedReader = new BufferedReader(new FileReader(new File(fileName)));
            fileParser.readFile(bufferedReader);
            fileParser.printMostOccurrenceWords();
            fileParser.printLeastOccurrenceWords();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testFileRead() {
        System.out.println("************** Testing File test2.txt ***************\n");
        FileParser fileParser = new FileParser();
        String fileName = getClass().getClassLoader().getResource("test2.txt").getFile();
        fileParser.readFile(fileName);
        fileParser.printMostOccurrenceWords();
        fileParser.printLeastOccurrenceWords();
    }


}
