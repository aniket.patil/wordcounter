package com.yactraq.wordcounter;

import java.io.*;
import java.util.*;

public class FileParser {

    //This field holds word and its occurrence count
    private Map<String, Integer> wordOccurrenceMap;
    //Sorted list of words
    private List<Map.Entry<String, Integer>> sortedWordList;

    //Regex to split sentence
    private final static String SENTENCE_SPLIT_REGEX = "\\s*(=>|,|\\s)\\s*";

    //Set of common words
    private static Set<String> commonWords;


    //Read Common words at initialization
    static {
        commonWords = new HashSet<String>();
        ClassLoader classLoader = ClassLoader.getSystemClassLoader();
        Scanner scanner;
        try {
            String fileName = classLoader.getResource("common_words.txt").getFile();
            scanner = new Scanner(new File(fileName));
            while (scanner.hasNextLine()) {
                String word = scanner.nextLine();
                commonWords.add(word.toLowerCase());
            }

            scanner.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }


    }


    //<Aniket Patil - Jan-2018>
    //This method reads file and update occurrence counter map
    public void readFile(BufferedReader bufferedReader) {

        wordOccurrenceMap = new HashMap<String, Integer>();
        String line = null;
        try {
            while ((line = bufferedReader.readLine()) != null) {
                String[] words = line.split(SENTENCE_SPLIT_REGEX);
                if (words != null && words.length > 0) {
                    for (String wordValue : words) {
                        wordValue = wordValue.toLowerCase();
                        if (!commonWords.contains(wordValue) && wordValue.length() > 0) {
                            //Not a common word
                            if (wordOccurrenceMap.containsKey(wordValue)) {
                                Integer count = wordOccurrenceMap.get(wordValue);
                                count = count + 1;
                                wordOccurrenceMap.put(wordValue, count);
                            } else {
                                //First occurrence
                                wordOccurrenceMap.put(wordValue, 1);
                            }
                        }
                    }
                }
            }

            // Sorting occurrence map
            sortedWordList = new ArrayList<Map.Entry<String, Integer>>(wordOccurrenceMap.entrySet());
            Collections.sort(sortedWordList, new Comparator<Map.Entry<String, Integer>>() {
                public int compare(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) {
                    return o1.getValue().compareTo(o2.getValue());
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    //<Aniket Patil - Jan-2018>
    //This method reads file
    public void readFile(String fileName) {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(fileName));
            readFile(reader);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    //<Aniket Patil - Jan-2018>
    //This print mostly used words in the file
    public void printMostOccurrenceWords() {

        List<Map.Entry<String, Integer>> wordList = getMostlyUsedWords();
        if (wordList != null) {

            System.out.println("Most Occurred Word(s)");
            System.out.println("**************************************");

            for (Map.Entry<String, Integer> entry : wordList) {
                System.out.println(entry.getKey() + " : " + entry.getValue());
            }

            System.out.println("**************************************\n");
        } else {

            System.out.println("Word list is empty");
        }
    }

    //<Aniket Patil - Jan-2018>
    //This print least used words in the file
    public void printLeastOccurrenceWords() {

        List<Map.Entry<String, Integer>> wordList = getLeastUsedWords();
        if (wordList != null) {

            System.out.println("Least Occurred Word(s)");
            System.out.println("**************************************");

            for (Map.Entry<String, Integer> entry : wordList) {
                System.out.println(entry.getKey() + " : " + entry.getValue());
            }

            System.out.println("**************************************\n");
        } else {

            System.out.println("Word list is empty");
        }
    }


    //<Aniket Patil - Jan-2018>
    //Returns mostly used word list
    public List<Map.Entry<String, Integer>> getMostlyUsedWords() {
        if (sortedWordList != null && !sortedWordList.isEmpty()) {
            int length = sortedWordList.size();
            int loopCount = 5;
            if (length < loopCount) {
                loopCount = length;
            }
            List<Map.Entry<String, Integer>> mostUsedWords = new ArrayList<Map.Entry<String, Integer>>(loopCount);

            for (int i = 1; i <= loopCount; i++) {
                Map.Entry<String, Integer> entry = sortedWordList.get(length - i);
                mostUsedWords.add(entry);
            }

            return mostUsedWords;

        }
        return null;
    }


    //<Aniket Patil - Jan-2018>
    //Returns least used word list
    public List<Map.Entry<String, Integer>> getLeastUsedWords() {
        if (sortedWordList != null && !sortedWordList.isEmpty()) {
            int length = sortedWordList.size();
            int loopCount = 5;
            if (length < loopCount) {
                loopCount = length;
            }
            List<Map.Entry<String, Integer>> leastUsedWords = new ArrayList<Map.Entry<String, Integer>>(loopCount);

            for (int i = 0; i < loopCount; i++) {
                Map.Entry<String, Integer> entry = sortedWordList.get(i);
                leastUsedWords.add(entry);
            }

            return leastUsedWords;

        }
        return null;
    }


}
